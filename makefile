all: raytracer

raytracer: main.o Camera.o lodepng.o Ray.o Scene.o SceneObject.o Sphere.o Cube.o Plane.o
	g++ main.o Camera.o lodepng.o Ray.o Scene.o SceneObject.o Sphere.o Cube.o Plane.o -o raytracer

main.o: main.cpp
	g++ -c main.cpp

Camera.o: Camera.cpp Camera.h
	g++ -c Camera.cpp

lodepng.o: lodepng.cpp lodepng.h
	g++ -c lodepng.cpp

Ray.o: Ray.cpp Ray.h
	g++ -c Ray.cpp 
	
SceneObject.o: SceneObject.cpp SceneObject.h
	g++ -c SceneObject.cpp

Scene.o: Scene.cpp Scene.h
	g++ -c Scene.cpp
	
Sphere.o: Sphere.cpp Sphere.h
	g++ -c Sphere.cpp
	
Cube.o: Cube.cpp Cube.h
	g++ -c Cube.cpp
	
Plane.o: Plane.cpp Plane.h
	g++ -c Plane.cpp
	
clean:
	rm -rf *o output.png raytracer
	
	
