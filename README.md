# Ray Tracer #

## Description ##

This ray tracer started as a class project for a graduate-level computer graphics course (CS 535 at the University of Kentucky), but has been extended beyond the class requirements.  A full feature list can be found below.  

## Project Setup / Installation ##

A Visual Studio 2010 project is included with the source.  In addition, a Windows executable is available under the Downloads tab.  

## Feature Set ##

* Phong shading
* Specular reflection
* Refraction
* Spherical and grid-shaped area lights.
* Soft shadows using jittered sampling
* Anti-aliasing using jittered sampling
* Attenuation through transparent objects with the Beer-Lambert law.
* Transformations
* Instancing

## Example Output ##

The output below utilizes all of the listed features.  On an Intel i7 at 2.0 GHz with two light sources, 4x4 soft shadow sampling, and 2x2 super sampling, the render below took 677 seconds.  

![output.png](https://bitbucket.org/repo/oAxRaL/images/3110866910-output.png)

## Planned Features ##

* A more robust class implementation of BRDFs so different ones can be swapped out easily.
* Glossy reflections  
* Procedural texturing
* An acceleration structure such as K-d trees or bounding interval hierarchies.  
* Triangle intersection
* Mesh loading
* General optimization improvements.
* Incremental rendering, so renders can be viewed in-progress.

## External Libraries ##
* GLM (GL Mathematics) - A math library used for matrix and vector operations.
* lodepng - An image library used for writing the final output to a .png.