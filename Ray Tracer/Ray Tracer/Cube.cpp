#include "Cube.h"
#include <iostream>


Cube::Cube(glm::vec3 min, glm::vec3 max) {
	this->min = min;
	this->max = max;
}


Cube::~Cube(void)
{
}

bool Cube::intersect(const Ray &ray, float &tHit) {
	glm::vec3 rayInv = 1.0f / ray.direction;

	double tx1 = (min.x - ray.origin.x) * rayInv.x;
	double tx2 = (max.x - ray.origin.x) * rayInv.x;
	double tmin = glm::min(tx1, tx2);
	double tmax = glm::max(tx1, tx2);

	double ty1 = (min.y - ray.origin.y) * rayInv.y;
	double ty2 = (max.y - ray.origin.y) * rayInv.y;
	tmin = glm::max(tmin, glm::min(ty1, ty2));
	tmax = glm::min(tmax, glm::max(ty1, ty2));

	double tz1 = (min.z - ray.origin.z) * rayInv.z;
	double tz2 = (max.z - ray.origin.z) * rayInv.z;
	tmin = glm::max(tmin, glm::min(tz1, tz2));
	tmax = glm::min(tmax, glm::max(tz1, tz2));

	if (tmax >= tmin) {
		if (tmin >= 0.0f && tmin >= ray.tmin && tmin <= ray.tmax) {
			tHit = tmin;
			return true;
		}
		else if (tmax > tmin && tmax <= ray.tmax && tmax >= ray.tmin) {
			tHit = tmax;
			return true;
		}
	}
	return false;
}


glm::vec3 Cube::getNormal(glm::vec3 position) {
	float epsilon = 0.001f;
	glm::vec3 normal;
	float xDelta = std::abs(std::abs(position.x) - max.x);
	float yDelta = std::abs(std::abs(position.y) - max.y);
	float zDelta = std::abs(std::abs(position.z) - max.z);

	float minDelta = glm::min(xDelta, glm::min(yDelta, zDelta));

	if (minDelta == xDelta) {
		if (std::abs(position.x - min.x) < epsilon) {
			normal = glm::vec3(-1, 0, 0);
		}
		else if (std::abs(position.x - max.x) < epsilon) {
			normal = glm::vec3(1, 0, 0);
		}
	}
	else if (minDelta == yDelta) {
		if (std::abs(position.y - min.y) < epsilon) {
			normal = glm::vec3(0, -1, 0);
		}
		else if (std::abs(position.y - max.y) < epsilon) {
			normal = glm::vec3(0, 1, 0);
		}
	}
	else if (minDelta == zDelta) {
		if (std::abs(position.z - min.z) < epsilon) {
			normal = glm::vec3(0, 0, -1);
		}
		else if (std::abs(position.z - max.z) < epsilon) {
			normal = glm::vec3(0, 0, 1);
		}
	}

	return normal;
}

glm::vec3 Cube::sample(int sampleNum, Sampler *sampler) {
	// TODO: Replace this with a different sampler mapping.
	return sampler->sampleUnitSquare(sampleNum);
}