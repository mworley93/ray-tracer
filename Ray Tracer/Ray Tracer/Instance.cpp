#include "Instance.h"



Instance::Instance(GeometricObject *object) {
	objectPtr = object;
	this->modelToWorld = glm::mat4(1.0f);
	this->worldToModel = glm::inverse(modelToWorld);
};

Instance::~Instance(void) {
	if (sampler) {
		delete sampler;
		sampler = NULL;
	}
};

void Instance::setMaterial(const Material &material) { 
	this->material = material; 
}

void Instance::setTransformation(const glm::mat4 &transform) {
	this->modelToWorld = transform;
	this->worldToModel = glm::inverse(transform);
}


bool Instance::intersect(const Ray &ray, float &tHit) {
	glm::vec4 transOrigin = worldToModel * glm::vec4(ray.origin, 1.0f);
	glm::vec4 transDir = worldToModel * glm::vec4(ray.direction, 0.0f);
	Ray localRay(glm::vec3(transOrigin), glm::vec3(transDir), ray.tmin, ray.tmax);
	return objectPtr->intersect(localRay, tHit);
}

glm::vec3 Instance::getNormal(glm::vec3 position) {
	glm::vec3 transPos = glm::vec3(worldToModel * glm::vec4(position, 1.0f));
	return glm::normalize(glm::vec3(glm::transpose(worldToModel) * glm::vec4(objectPtr->getNormal(transPos), 0.0f)));
}

glm::vec3 Instance::sample(int sampleNum) {
	return glm::vec3(modelToWorld * glm::vec4(objectPtr->sample(sampleNum, sampler), 1.0f));
}