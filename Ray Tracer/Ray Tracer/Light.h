#pragma once
#include "ext_libs/glm/glm/glm.hpp"
#include <stdlib.h>
#include "Instance.h"

// This class represents an area light object.  
class Light {
public:
	// Constructor.
	// Params:
	//		intensity - The color of the light.
	//		shape - An instance of a geometric object representing the shape of the light.
	Light(glm::vec4 intensity, Instance *shape) { 
		this->intensity = intensity;
		this->geometricObj = shape;
	}

	glm::vec4 intensity;
	float attenuationConstant;
	Instance *geometricObj;

	glm::vec3 getCenter() { return glm::vec3(geometricObj->modelToWorld * glm::vec4(center, 1.0f)); };

private:
	glm::vec3 center;
};