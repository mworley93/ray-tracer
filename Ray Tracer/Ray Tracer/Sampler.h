#pragma once
#include "ext_libs/glm/glm/glm.hpp"
#include <stdlib.h>
#include <vector>

// The base class for all types of sampling routines.  
class Sampler
{
public:
	Sampler(void) {};
	Sampler(int samplesX, int samplesY) {};
	virtual ~Sampler(void) {};

	// Abstract routine that returns a sample from a unit square.
	// Params:
	//		sampleNum - The sample number.
	virtual glm::vec3 sampleUnitSquare(int sampleNum) = 0;

	// Abstract routine that returns a sample from a unit sphere.
	// Params:
	//		sampleNum - The sample number.
	virtual glm::vec3 sampleUnitSphere(int sampleNum) = 0;

	float numSamples; // The number of samples.

protected:
	float mDeltaX;
	float mDeltaY;
	int mSamplesX;
	int mSamplesY;
};

