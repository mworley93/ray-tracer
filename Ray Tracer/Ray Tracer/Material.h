#include "ext_libs/glm/glm/glm.hpp"

// This struct represents a material.  Holds information needed when shading an object, such as color and shininess.
struct Material {
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::vec4 refraction;
	float shininess;
	float ior;
	float glossiness;
	float absorbance;
	bool isDielectric;

	Material() {
		ambient = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
		diffuse = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		specular = glm::vec4(0.4f, 0.4f, 0.4f, 1.0f);
		refraction = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		shininess = 0.0f;
		ior = 0.0f;
		glossiness = 0.5f;
		absorbance = 1.0f;
		isDielectric = true;
	}

	Material(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 refraction , float shininess, float ior, float glossiness, float absorbance, 
		bool isDielectric) {
		this->ambient = ambient;
		this->diffuse = diffuse;
		this->specular = specular;
		this->refraction = refraction;
		this->shininess = shininess;
		this->ior = ior;
		this->glossiness = glossiness;
		this->absorbance = absorbance;
		this->isDielectric = isDielectric;
	}
	
	bool isRefractive() { return (absorbance < 1.0f); }

	bool isReflective() { return (glossiness > 0.0f); }
};
