#pragma once
#include "GeometricObject.h"

// This class represents a cube primitive.
class Cube :
	public GeometricObject
{
public:
	Cube(glm::vec3 min = glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec3 max = glm::vec3(1.0f, 1.0f, 1.0f));
	~Cube(void);

	bool intersect(const Ray &ray, float &tHit);
	glm::vec3 getNormal(glm::vec3 position);
	glm::vec3 sample(int sampleNum, Sampler *sampler);

private:
	glm::vec3 min; // Lower left coordinate.
	glm::vec3 max; // Top right coordinate.
};

