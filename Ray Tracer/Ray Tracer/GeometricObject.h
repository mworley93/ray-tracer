#pragma once
#include "Ray.h"
#include "ext_libs/glm/glm/glm.hpp"
#include "Material.h"
#include "Sampler.h"
#include <cmath>

// This is the superclass for all objects that can be rendered (primitives and meshes).  
class GeometricObject
{
public:
	GeometricObject(void) {}
	virtual ~GeometricObject(void) {}
	
	// Abstract method defining the intersection routine.
	virtual bool intersect(const Ray &ray, float &tHit) = 0;

	// Abstract method defining the normal calculation routine.
	virtual glm::vec3 getNormal(glm::vec3 position) = 0;

	// Abstract method defining the sampling routine behavior.
	virtual glm::vec3 sample(int sampleNum, Sampler *sampler) = 0;
};

