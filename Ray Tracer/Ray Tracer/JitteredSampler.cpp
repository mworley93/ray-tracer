#include "JitteredSampler.h"

JitteredSampler::JitteredSampler(void) {
	mDeltaX = 1.0f;
	mDeltaY = 1.0f;
	numSamples = 1;
	mSamplesX = 1;
	mSamplesY = 1;
}

JitteredSampler::JitteredSampler(int samplesX, int samplesY) {
	mDeltaX = 1.0f / samplesX;
	mDeltaY = 1.0f / samplesY;
	numSamples = samplesX * samplesY;
	mSamplesX = samplesX;
	mSamplesY = samplesY;
}

JitteredSampler::~JitteredSampler(void)
{
}

glm::vec3 JitteredSampler::sampleUnitSquare(int sampleNum) {
	float offsetX = rand() % 100 / 100.0f * mDeltaX;
	float offsetY = rand() % 100 / 100.0f * mDeltaY;
	glm::vec3 min = glm::vec3(-0.5f, -0.5f, 0.0f);
	glm::vec3 max = glm::vec3(0.5f, 0.5f, 0.0f);
	glm::vec3 position(min.x + (sampleNum % mSamplesX) * mDeltaX + offsetX, max.y - (sampleNum / mSamplesX) * mDeltaY - offsetY, min.z);
	return position;
}

glm::vec3 JitteredSampler::sampleUnitSphere(int sampleNum) {
	// Calculate a jittered sample in a uniform grid from u,v = 0 to 1.
	float offsetX = rand() % 100 / 100.0f * mDeltaX;
	float offsetY = rand() % 100 / 100.0f * mDeltaY;
	glm::vec3 min;
	glm::vec3 max = glm::vec3(1.0f);
	float u = min.x + (sampleNum % mSamplesX) * mDeltaX + offsetX;
	float v = min.y + (sampleNum / mSamplesX) * mDeltaY + offsetY;

	// Map U and V to spherical coordinates and then return the final position in Cartesian coordinates.
	float theta = 2.0f * 3.14f * u;
	float phi = glm::acos(2.0f * v - 1.0f);
	return glm::vec3(glm::sin(theta) * glm::cos(phi), glm::sin(theta) * glm::sin(phi), glm::cos(theta));
}
