#pragma once
#include "ext_libs/glm/glm/glm.hpp"
#include "ext_libs/glm/glm/gtx/transform.hpp"
#include <stack>

// A matrix stack that is used for accumulating transformations.  
class MatrixStack {
public:
	// Default constructor. Initializes the current matrix to the identity.
	MatrixStack(void);

	// Constructor.  Initializes the current matrix to the one given.
	MatrixStack(const glm::mat4 &currentMatrix);

	~MatrixStack(void);

	// Returns the current matrix.
	const glm::mat4 &top() const;

	// Replaces the current matrix with the identity matrix;
	void loadIdentity();

	// Multiplies a transformation matrix against the current matrix.
	void translate(float x, float y, float z);

	// Multiplies a scale matrix against the current matrix.
	void scale(float x, float y, float z);

	// Multiples a rotation matrix against the current matrix.
	void rotate(float angle, float x, float y, float z);

	// Pushes the current matrix onto the matrix stack.
	void pushStack();

	// Pops the current matrix, replacing it with the top of the matrix stack.
	void popStack();

private:
	std::stack<glm::mat4> mMatrixStack;
	glm::mat4 mCurrentMatrix;
};

