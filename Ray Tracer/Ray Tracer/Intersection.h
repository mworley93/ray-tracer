#pragma once
#include "Instance.h"

// A simple object that holds intersection information.
struct Intersection {
	bool operator < (const Intersection &a) { return (this->tHit < a.tHit);}

	Intersection(Instance *objectHit, float tHit) {
		this->objectHit = objectHit;
		this->tHit = tHit;
	}

	~Intersection(void) {};

	Instance *objectHit;
	float tHit;
};

