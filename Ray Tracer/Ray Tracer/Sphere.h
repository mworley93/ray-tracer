#pragma once
#include "GeometricObject.h"

// This class represents the sphere primitive.
class Sphere :
	public GeometricObject
{
public:
	Sphere();
	Sphere(glm::vec3 center, float radius);
	~Sphere(void);

	void setRadius(float radius) { mRadius = radius; };
	void setCenter(glm::vec3 center) { mCenter = center; };

	// Checks the intersection of a ray and a sphere.
	// http://scratchapixel.com/lessons/3d-basic-lessons/lesson-7-intersecting-simple-shapes/ray-sphere-intersection/
	bool intersect(const Ray &ray, float &tHit);

	glm::vec3 getNormal(glm::vec3 position);

	glm::vec3 sample(int sampleNum, Sampler *sampler);

private:
	glm::vec3 mCenter;
	float mRadius;
	float mRadiusSquared;
};

