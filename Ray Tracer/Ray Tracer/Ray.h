#pragma once

#include "Camera.h"

// This class reprents a viewing ray.
class Ray
{
public:
	// Creates a ray, given the x and y coordinates in screen space and the camera parameters.
	Ray(float x, float y, int width, int height, const Camera &camera);

	// Creates a ray, given the ray origin, direction, and min and max t-values.
	Ray(glm::vec3 origin, glm::vec3 direction, float tmin, float tmax);

	~Ray(void);

	// Ray origin and direction in world space.
	glm::vec3 origin; 
	glm::vec3 direction;

	// t-max and t-min in world space. Based on the camera's near and far clipping planes.
	float tmax;
	float tmin;
};

