#include "Scene.h"


Scene::Scene(void)
{
}


Scene::~Scene(void) {
	for (int i = 0; i < objects.size(); i++) {
		delete objects[i];
	}
	clear();
}

void Scene::clear() {
	objects.clear();
	lights.clear();
}

void Scene::buildScene() {
	MatrixStack matrixStack;

	// Geometric objects
	Cube *cube = new Cube();
	Sphere *sphere = new Sphere();

	// Light object
	Instance *windowLightObject = new Instance(cube);
	matrixStack.translate(-7.0f, 3, 0.5f);
	matrixStack.rotate(3.14f/2.0f, 0, 1.0f, 0);
	matrixStack.scale(1.5f, 1.0f, 1.0f);
	windowLightObject->setTransformation(matrixStack.top());

	Instance *interiorLightObject = new Instance(sphere);
	matrixStack.loadIdentity();
	matrixStack.translate(0.0f, 3.5f, 1.0f);
	matrixStack.scale(0.5f, 0.5f, 0.5f);
	interiorLightObject->setTransformation(matrixStack.top());

	JitteredSampler *lightSampler = new JitteredSampler(4, 4);
	windowLightObject->sampler = lightSampler;
	interiorLightObject->sampler = lightSampler;

	Light *windowLight = new Light(glm::vec4(0.8f, 0.8f, 0.8f, 1.0f), windowLightObject);
	windowLight->attenuationConstant = 0.0f;
	lights.push_back(windowLight);

	Light *interiorLight = new Light(glm::vec4(0.3f, 0.3f, 0.3f, 1.0f), interiorLightObject);
	interiorLight->attenuationConstant = 0.0f;
	lights.push_back(interiorLight);

	// Materials
	Material silverMat(glm::vec4(0.19225, 0.19225, 0.19225, 1.0), glm::vec4(0.50754, 0.50754, 0.50754, 1.0), glm::vec4(0.508273, 0.508273, 0.508273, 1.0f), 
		glm::vec4(0.0f), 51.2f, 50.0f, 1.0f, 0.0f, false); 
	Material glassMat(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.1f, 0.1f, 0.1f, 0.1f), glm::vec4(0.9f, 0.9f, 0.9f, 1.0f), 
		glm::vec4(1.0f), 96.0f, 1.5f, 1.0f, 0.0f, true);
	Material tintedGlassMat(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.2f, 0.2f, 0.2f, 1.0f), glm::vec4(0.9f, 0.9f, 0.9f, 1.0f), 
		glm::vec4(0.5f, 0.5f, 1.0f, 1.0f), 96.0f, 1.5f, 1.0f, 0.1f, true);
	Material rubyMat(glm::vec4(0.1745f, 0.01175f, 0.01175f, 0.55f), glm::vec4(0.61424f, 0.04136f, 0.04136f, 0.55f), glm::vec4(0.727811f, 0.626959f, 0.626959f, 0.55f), 
		glm::vec4(0.61424f, 0.04136f, 0.04136f, 0.55f), 78.6f, 1.76f, 1.0f, 0.5f, true);
	Material emeraldMat(glm::vec4(0.0215f, 0.1745f, 0.0215f, 0.55f), glm::vec4(0.07568f, 0.61424f, 0.07568f, 0.55f), glm::vec4(0.633f, 0.727811f, 0.633f, 0.55f), 
		glm::vec4(0.3f), 78.6f, 2.3f, 1.0f, 1.0f, true);
	Material floorMat(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), glm::vec4(0.3f, 0.3f, 0.3f, 0.5f), 
		glm::vec4(0.0f), 32.0f, 2.0f, 1.0f, 1.0f, true);
	Material wallMat(glm::vec4(0.4f, 0.4f, 0.2f, 1.0f), glm::vec4(1.0f, 1.0f, 0.9f, 1.0f), glm::vec4(0.1f, 0.1f, 0.1f, 0.0f), 
		glm::vec4(0.0f), 10.0f, 2.0f, 0.1f, 1.0f, true);
	Material mirrorMat(glm::vec4(0.1f, 0.1f, 0.1f, 1.0f), glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(1.0f, 1.0f, 1.0f, 0.8f), 
		glm::vec4(0.0f), 51.2f, 0.18f, 1.0f, 0.0f, false);
	Material woodMat(glm::vec4(0.2f, 0.1f, 0.1f, 1.0f), glm::vec4(0.7f, 0.5f, 0.3f, 1.0f), glm::vec4(0.1f, 0.1f, 0.1f, 1.0f), 
		glm::vec4(0.0f), 32.0f, 2.0f, 1.0f, 1.0f, true);

	// Add objects to render.

	// Left wall
	Instance *leftWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(0, 3, 6);
	matrixStack.scale(6, 3, 0.5f);
	leftWall->setMaterial(wallMat);
	leftWall->setTransformation(matrixStack.top());
	objects.push_back(leftWall);

	// Right wall
	Instance *rightWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(0, 3, -6);
	matrixStack.scale(6, 3, 0.5f);
	rightWall->setMaterial(wallMat);
	rightWall->setTransformation(matrixStack.top());
	objects.push_back(rightWall);

	// Front wall
	Instance *frontWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(5.5, 3, 0);
	matrixStack.scale(0.5f, 3, 6);
	frontWall->setMaterial(wallMat);
	frontWall->setTransformation(matrixStack.top());
	objects.push_back(frontWall);

	// Bottom back wall
	Instance *bottomBackWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-5.5f, 1, 0);
	matrixStack.scale(0.5f, 1, 6);
	bottomBackWall->setMaterial(wallMat);
	bottomBackWall->setTransformation(matrixStack.top());
	objects.push_back(bottomBackWall);

	// Top back wall
	Instance *topBackWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-5.5f, 5, 0);
	matrixStack.scale(0.5f,1,6);
	topBackWall->setMaterial(wallMat);
	topBackWall->setTransformation(matrixStack.top());
	objects.push_back(topBackWall);

	// Left back wall
	Instance *leftBackWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-5.5f,3,4);
	matrixStack.scale(0.5f, 1,2);
	leftBackWall->setMaterial(wallMat);
	leftBackWall->setTransformation(matrixStack.top());
	objects.push_back(leftBackWall);

	// Right back wall
	Instance *rightBackWall = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-5.5f,3,-3.5f);
	matrixStack.scale(0.5f, 1, 2.5f);
	rightBackWall->setMaterial(wallMat);
	rightBackWall->setTransformation(matrixStack.top());
	objects.push_back(rightBackWall);

	// Window
	Instance *window = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-5.4f,3.0f,0.4f);
	matrixStack.scale(0.2f, 1.0f, 2.0f);
	window->setMaterial(glassMat);
	window->setTransformation(matrixStack.top());
	objects.push_back(window);

	// Floor
	Instance *floor = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-0.5f, 0, 0);
	matrixStack.scale(6.25f, 0.5f, 6.25f);
	floor->setMaterial(floorMat);
	floor->setTransformation(matrixStack.top());
	objects.push_back(floor);

	// Ceiling
	Instance *ceiling = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-0.5f, 6, 0.5f);
	matrixStack.scale(6.25f, 0.5, 6.5f);
	ceiling->setMaterial(floorMat);
	ceiling->setTransformation(matrixStack.top());
	objects.push_back(ceiling);

	// Table
	Instance *table = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-2, 1.5f, 1.5f);
	matrixStack.scale(1, 0.1f, 1.5f);
	table->setMaterial(glassMat);
	table->setTransformation(matrixStack.top());
	objects.push_back(table);

	// Table legs
	Instance *tableLeg1 = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-1.1f, 0.9f, 2.9f);
	matrixStack.scale(0.1f, 0.5f, 0.1f);
	tableLeg1->setMaterial(woodMat);
	tableLeg1->setTransformation(matrixStack.top());
	objects.push_back(tableLeg1);

	Instance *tableLeg2 = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-1.1f, 0.9f, 0.1f);
	matrixStack.scale(0.1f, 0.5f, 0.1f);
	tableLeg2->setMaterial(woodMat);
	tableLeg2->setTransformation(matrixStack.top());
	objects.push_back(tableLeg2);

	Instance *tableLeg3 = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-2.9f, 0.9f, 2.9f);
	matrixStack.scale(0.1f, 0.5f, 0.1f);
	tableLeg3->setMaterial(woodMat);
	tableLeg3->setTransformation(matrixStack.top());
	objects.push_back(tableLeg3);

	Instance *tableLeg4 = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-2.9f, 0.9f, 0.1f);
	matrixStack.scale(0.1f, 0.5f, 0.1f);
	tableLeg4->setMaterial(woodMat);
	tableLeg4->setTransformation(matrixStack.top());
	objects.push_back(tableLeg4);

	// Shelves
	Instance *shelf1 = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-4.5f, 3, 3.0f);
	matrixStack.scale(0.2f, 0.05f, 0.6f);
	shelf1->setMaterial(woodMat);
	shelf1->setTransformation(matrixStack.top());
	objects.push_back(shelf1);

	Instance *shelf2 = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-4.5f, 2.3f, 3.0f);
	matrixStack.scale(0.2f, 0.05f, 0.6f);
	shelf2->setMaterial(woodMat);
	shelf2->setTransformation(matrixStack.top());
	objects.push_back(shelf2);

	// Mirror
	Instance *mirror = new Instance(cube);
	matrixStack.loadIdentity();
	matrixStack.translate(-2, 3, 5.5f);
	matrixStack.scale(1, 1, 0.25f);
	mirror->setMaterial(mirrorMat);
	mirror->setTransformation(matrixStack.top());
	objects.push_back(mirror);

	// Spheres
	Instance *glassBall = new Instance(sphere);
	matrixStack.loadIdentity();
	matrixStack.translate(-1.7f, 2, 0.4f);
	matrixStack.scale(0.4f, 0.4f, 0.4f);
	glassBall->setMaterial(silverMat);
	glassBall->setTransformation(matrixStack.top());
	objects.push_back(glassBall);

	Instance *shinyBall1 = new Instance(sphere);
	matrixStack.loadIdentity();
	matrixStack.translate(-2, 1.9f, 2.5f);
	matrixStack.scale(0.3f, 0.3f, 0.3f);
	shinyBall1->setMaterial(rubyMat);
	shinyBall1->setTransformation(matrixStack.top());
	objects.push_back(shinyBall1);

	Instance *glassBall2 = new Instance(sphere);
	matrixStack.loadIdentity();
	matrixStack.translate(-1.5f, 1.95f, 1.4f);
	matrixStack.scale(0.35f, 0.35f, 0.35f);
	glassBall2->setMaterial(tintedGlassMat);
	glassBall2->setTransformation(matrixStack.top());
	objects.push_back(glassBall2);
}
