#pragma once
#include "sampler.h"

// This subclass implements regular sampling.  In regular sampling, a grid based on the number of samples is created and a sample for the center of 
// each grid square is returned. 
class RegularSampler :
	public Sampler
{
public:
	RegularSampler(void);
	RegularSampler(int samplesX, int samplesY);

	~RegularSampler(void);

	glm::vec3 sampleUnitSquare(int sampleNum);
	glm::vec3 sampleUnitSphere(int sampleNum);
};

