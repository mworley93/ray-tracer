#pragma once
#include "GeometricObject.h"

// This class represents a single instance of a geometry.  It retains a pointer to the original geometry, but has a 
// unique material, transformation, and sampler.
class Instance
{
public:
	Instance(GeometricObject *object);
	~Instance(void);

	bool intersect(const Ray &ray, float &tHit);

	glm::vec3 getNormal(glm::vec3 position);

	void setMaterial(const Material &material);

	void setTransformation(const glm::mat4 &transform);

	glm::vec3 sample(int sampleNum);

	Material material;
	glm::mat4 modelToWorld;
	glm::mat4 worldToModel;
	Sampler *sampler;

private:
	GeometricObject *objectPtr;
};

