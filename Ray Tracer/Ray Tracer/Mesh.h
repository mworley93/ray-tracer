#pragma once
#include "GeometricObject.h"

class Mesh :
	public GeometricObject
{
public:
	Mesh(void);
	~Mesh(void);
	bool intersect(const Ray &ray, float &tHit);
	glm::vec3 sample(int sampleNum, Sampler *sampler);

	glm::mat4 modelToWorld;
	float *vertices;
	float *indices;
};

