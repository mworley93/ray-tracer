#include "Camera.h"

Camera::Camera() {
	mFov = 90;
	mAspect = 1;
	mNear = 1;
	mFar = 10;
}

Camera::Camera(float fov, float aspect, float near, float far) {
	mFov = glm::radians(fov);
	mAspect = aspect;
	mNear = near;
	mFar = far;
}

Camera::~Camera(void) {
}

void Camera::lookAt(const glm::vec3 &eye, const glm::vec3 &center, const glm::vec3 &up) {
	glm::vec3 upDir = glm::normalize(up);
	glm::vec3 lookDir = glm::normalize(center - eye);
	glm::vec3 rightDir = glm::normalize(glm::cross(lookDir, upDir));
	glm::vec3 orthoUpDir = glm::cross(rightDir, lookDir);

	glm::mat4 rotation(1.0f);
	rotation[0] = glm::vec4(rightDir, 0.0f);
	rotation[1] = glm::vec4(orthoUpDir, 0.0f);
	rotation[2] = glm::vec4(-lookDir, 0.0f);

	rotation = glm::transpose(rotation);

	glm::mat4 translation(1.0f);
	translation[3] = glm::vec4(-eye, 1.0f);
	mWorldToCamera = rotation * translation;
	mCameraToWorld = glm::inverse(mWorldToCamera);
}