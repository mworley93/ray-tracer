#include "Plane.h"


Plane::Plane(glm::vec3 position, glm::vec3 normal) {
	mPosition = position;
	mNormal = normal;
}


Plane::~Plane(void)
{
}

bool Plane::intersect(const Ray &ray, float &tHit) {
 	float epsilon = 0.00001f; 
	float denom = glm::dot(ray.direction, mNormal);
	if (abs(denom) > epsilon) {
		float t = glm::dot(mPosition - ray.origin, mNormal) / denom;
		if (t > ray.tmin) {
			tHit = t;
			return true;
		}
	}
	return false;
}

glm::vec3 Plane::getNormal(glm::vec3 position) {
	return mNormal;
}

glm::vec3 Plane::sample(int sampleNum, Sampler *sampler) {
	return sampler->sampleUnitSquare(sampleNum);
}
