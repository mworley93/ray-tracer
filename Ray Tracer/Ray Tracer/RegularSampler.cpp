#include "RegularSampler.h"

RegularSampler::RegularSampler(void) {
	mDeltaX = 1.0f;
	mDeltaY = 1.0f;
	numSamples = 1;
	mSamplesX = 1;
	mSamplesY = 1;
}

RegularSampler::RegularSampler(int samplesX, int samplesY) {
	mDeltaX = 1.0f / samplesX;
	mDeltaY = 1.0f / samplesY;
	numSamples = samplesX * samplesY;
	mSamplesX = samplesX;
	mSamplesY = samplesY;
}

RegularSampler::~RegularSampler(void)
{
}

glm::vec3 RegularSampler::sampleUnitSquare(int sampleNum) {
	glm::vec3 min = glm::vec3(-0.5f, -0.5f, 0.0f);
	glm::vec3 max = glm::vec3(0.5f, 0.5f, 0.0f);
	glm::vec3 position(min.x + (sampleNum % mSamplesX + 0.5f) * mDeltaX, max.y - (sampleNum / mSamplesX + 0.5f) * mDeltaY, min.z);
	return position;
}

glm::vec3 RegularSampler::sampleUnitSphere(int sampleNum) {
	// Calculate a regular sample in a uniform grid from U,V = 0 to 1.
	glm::vec3 min;
	glm::vec3 max = glm::vec3(1.0f);
	float u = min.x + (sampleNum % mSamplesX + 0.5f) * mDeltaX;
	float v = min.y + (sampleNum / mSamplesX + 0.5f) * mDeltaY;

	// Map (U,V) to spherical coordinates and then return the final position in Cartesian coordinates.
	float theta = 2.0f * 3.14f * u;
	float phi = glm::acos(2.0f * v - 1.0f);
	return glm::vec3(glm::sin(theta) * glm::cos(phi), glm::sin(theta) * glm::sin(phi), glm::cos(theta));
}