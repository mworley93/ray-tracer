#pragma once
#include "GeometricObject.h"

// This class represents the plane primitive.
class Plane :
	public GeometricObject
{
public:
	Plane(glm::vec3 position = glm::vec3(0,0,0), glm::vec3 normal = glm::vec3(0,1,0));
	~Plane(void);

	bool intersect(const Ray &ray, float &tHit);
	glm::vec3 getNormal(glm::vec3 position);
	glm::vec3 sample(int sampleNum, Sampler *sampler);

private:
	glm::vec3 mPosition;
	glm::vec3 mNormal;
};

