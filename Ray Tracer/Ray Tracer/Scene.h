#pragma once
#include <vector>
#include "Camera.h"
#include "Light.h"
#include "Instance.h"
#include "JitteredSampler.h"
#include "MatrixStack.h"
#include "Cube.h"
#include "Sphere.h"

// This class represents a scene, or a collection of a camera, lights, and objects to render.
class Scene {
public:
	Scene(void);
	~Scene(void);

	// Clears all objects in the scene.
	void clear();

	// Initializes the scene (lights, camera, and objects to render).  
	void buildScene();

	Camera camera;
	std::vector<Light*> lights;
	std::vector<Instance*> objects;
};

