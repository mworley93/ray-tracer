#pragma once
#include "ext_libs/glm/glm/glm.hpp"

// This class represents a camera in the world.  It allows you to dynamically change your viewing position, as well 
// as the aspect ratio, field of view, and near and far clipping planes.
class Camera
{
public:
	Camera();

	// Constructor.
	// Params:
	//		fov - The field of view.
	//		aspect - The aspect ratio.
	//		near - The near clipping plane.
	//		far - The far clipping plane.
	Camera(float fov, float aspect, float near, float far);

	// Destructor.
	~Camera(void);

	// Points the camera sitting at position eye towards position center, with an up vector given by up.
	void lookAt(const glm::vec3 &eye, const glm::vec3 &center, const glm::vec3 &up);

	float getFOV() const { return mFov; };
	float getAspect() const { return mAspect; };
	float getNearPlane() const { return mNear; };
	float getFarPlane() const { return mFar; };
	void getCameraToWorldMat(glm::mat4 &mat) const { mat = mCameraToWorld; };
	void getWorldToCamera(glm::mat4 &mat) const { mat = mWorldToCamera; };

private:
	float mFov;
	float mAspect;
	float mNear;
	float mFar;
	glm::mat4 mCameraToWorld;
	glm::mat4 mWorldToCamera;
};

