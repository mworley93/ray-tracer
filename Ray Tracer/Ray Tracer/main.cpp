
#include <iostream>
#include "lodepng.h"
#include "ext_libs/glm/glm/glm.hpp"
#include "Ray.h"
#include "Sphere.h"
#include "Scene.h"
#include "Plane.h"
#include "Instance.h"
#include "Light.h"
#include <time.h>
#include <iostream>
#include "MatrixStack.h"
#include "Instance.h"
#include "Cube.h"
#include "JitteredSampler.h"
#include "RegularSampler.h"
#include "Intersection.h"
#include <algorithm>

// Function prototypes.
void setupScene(Scene &scene);
void render(const Scene &scene);
glm::vec4 trace(Ray ray, const Scene &scene, int depth, float refractIndex);
glm::vec4 shade(const Scene &scene, Ray ray, float t, Instance *object, int depth, float refractIndex);
glm::vec3 reflect(const Ray &ray, const glm::vec3 &normal);
glm::vec3 refract(const Ray &ray, const glm::vec3 &normal, float n1, float n2, float cosI);
float getReflectance(const glm::vec3 &incident, const glm::vec3 &normal, float n1, float n2, float cosI);
glm::vec4 calculateBeerLambert(const Ray &ray, float t, const Material &material);
glm::vec4 calculateShadowAttenuation(const Scene &scene, const Ray &ray, const glm::vec3 &position, Light *light, float prevRefractIndex); 
void intersectClosest(const Scene &scene, const Ray &ray, Instance *&objectHit, float &tClosestHit);
void intersectAll(const Scene &scene, const Ray &ray, std::vector<Intersection> &intersections);
void setPixel(std::vector<unsigned char> &image, int x, int y, glm::vec4 color);
void writeImageToDisk(const char *filename, std::vector<unsigned char>& image);

// Image constants.
const int IMAGE_WIDTH = 768;  // Final image width.
const int IMAGE_HEIGHT = 512;  // Final image height.

const glm::vec4 BACKGROUND_COLOR = glm::vec4(0.5f, 0.8f, 1.0f, 1.0f);  // Background color of the image.
const int MAX_DEPTH = 5; // Max recursion depth
const float AIR_REFRACTION = 1.0f;
const float EPSILON = 0.0001f;

int main(int argc, char** argv) {
	srand(time(NULL));
	Scene scene;

	// Set up the camera.
	Camera camera(45, (float)IMAGE_WIDTH / (float)IMAGE_HEIGHT, 1, 200);
	camera.lookAt(glm::vec3(2.0f, 3.0f, -2.0f), glm::vec3(-3.0f, 1.5f, 2.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	scene.camera = camera;

	// Build the scene.  Building is currently static, as model loading or scene graph parsing is not yet supported.
	scene.buildScene();
	
	// Render the scene.
	clock_t begin = clock();
	render(scene);
	clock_t end = clock();
	double elapsed = double(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Rendering time: " << elapsed << " s" << std::endl;
	return 0;
}

// Renders the image.
// Params:
//		scene - The scene to render.
void render(const Scene &scene) {
	std::vector<unsigned char> image;
	image.resize(IMAGE_WIDTH * IMAGE_HEIGHT * 4);
	Camera camera = scene.camera;
	JitteredSampler *viewSampler = new JitteredSampler(2, 2);
	for (int i = 0; i < IMAGE_HEIGHT; i++) {
		for (int j = 0; j < IMAGE_WIDTH; j++) {
			glm::vec4 totalColor;
			for (int sample = 0; sample < viewSampler->numSamples; sample++) {
				glm::vec3 viewPos = viewSampler->sampleUnitSquare(sample);
				Ray ray(j + viewPos.x, i + viewPos.y, IMAGE_WIDTH, IMAGE_HEIGHT, camera);
				totalColor += trace(ray, scene, 0, AIR_REFRACTION);
			}
			setPixel(image, j, i, totalColor / (float)(viewSampler->numSamples));
		}
	}
	delete viewSampler;
	writeImageToDisk("output.png", image);
}

// Traces a single ray through a scene.
// Params:
//		ray - The ray to trace.
//		scene - The scene to trace the ray through.
//		depth - The current recursion depth.
//		refractIndex - The current refraction index.
// Returns:
//		The color for that pixel.
glm::vec4 trace(Ray ray, const Scene &scene, int depth, float refractIndex) {
	Instance *objectHit = NULL;
	glm::vec4 objectColor;
	float tClosestHit = ray.tmax;
	intersectClosest(scene, ray, objectHit, tClosestHit);
    if (objectHit != NULL && tClosestHit < ray.tmax) {
		return shade(scene, ray, tClosestHit, objectHit, depth, refractIndex);
	}
	else {
		return BACKGROUND_COLOR;
	}
}

// Shades an object that has been hit.
// Params:
//		scene - The scene being rendered.
//		ray - The current ray being traced.
//		t - The parameter t of the intersection point.
//		object - The object that was hit.
//		depth - The current recursion depth.
//		refractIndex - The current refraction index.
// Returns: 
//		The resulting color.
glm::vec4 shade(const Scene &scene, Ray ray, float t, Instance *object, int depth, float prevRefractIndex) {
	std::vector<Light*> lights = scene.lights;
	glm::vec3 position = ray.origin + ray.direction*t;
	glm::vec3 eye = -ray.direction;
	glm::vec3 normal = object->getNormal(position);
	glm::vec4 totalColor = object->material.ambient;
	for (int i = 0; i < lights.size(); i++) {
		Light *light = lights[i];
		glm::vec3 lightDir = glm::normalize(light->getCenter() - position);
		// Generate shadow ray
		glm::vec4 shadowAtten = calculateShadowAttenuation(scene, ray, position, light, prevRefractIndex);

		// Phong shading.
		glm::vec3 h = glm::normalize(eye + lightDir);
		float attenuation = 1.0f / (1.0f + light->attenuationConstant * (float)glm::length(light->getCenter() - position));
		float lambertFactor = glm::clamp(glm::dot(normal, lightDir), 0.0f, 1.0f);
		float specularFactor = std::pow(glm::max(0.0f, glm::dot(normal, h)), object->material.shininess);
		totalColor += light->intensity * shadowAtten * attenuation * (object->material.diffuse * lambertFactor + object->material.specular * specularFactor);
	}

	if (depth < MAX_DEPTH) {
		if (object->material.isRefractive() || object->material.isReflective()) {
			float n1, n2;
			glm::vec4 absorbance = glm::vec4(1.0f);
			float cosI = glm::dot(ray.direction, normal);
			if (cosI <= 0.0f) {
				n1 = prevRefractIndex;
				n2 = object->material.ior;
				cosI = -cosI;
			}
			else {
				n1 = object->material.ior;
				n2 = prevRefractIndex;
				normal = -normal;
				absorbance = calculateBeerLambert(ray, t, object->material);
			}
			float reflectivePercent = getReflectance(ray.direction, normal, n1, n2, cosI);
			float refractivePercent = 0.0f;
			if (object->material.isDielectric && object->material.isRefractive()) {
				refractivePercent = 1.0f - reflectivePercent;
			}

			glm::vec4 reflectiveColor;
			glm::vec4 refractiveColor;
			if (refractivePercent > 0.0f) {
				glm::vec3 T = refract(ray, normal, n1, n2, cosI);
				Ray refractedRay(position + T * EPSILON, T, 0.0f, ray.tmax);
				refractiveColor = object->material.refraction * (1-object->material.absorbance) * trace(refractedRay, scene, depth+1, n1);
			}
			if (reflectivePercent > 0.0f) {
				glm::vec3 R = reflect(ray, normal);
				Ray reflectedRay(position + R * EPSILON, R, 0.0f, ray.tmax);
				reflectiveColor = object->material.specular * object->material.glossiness * trace(reflectedRay, scene, depth+1, n1);
			}
			totalColor += reflectivePercent * reflectiveColor + refractivePercent * refractiveColor;
		}
	}
	return totalColor;
}

// Calculates the reflection vector.
// Params:
//		ray - The incident ray.
//		normal - The normal at the intersection point.
// Returns:
//		The normalized reflection vector.
glm::vec3 reflect(const Ray &ray, const glm::vec3 &normal) {
	glm::vec3 R = glm::normalize(ray.direction - 2.0f * glm::dot(ray.direction, normal) * normal);
	return R;
}

// Calculates the refraction vector.
// Params:
//		ray - The incident ray.
//		normal - The normal at the intersection point.
//		n1 - The IOR of the exiting medium.
//		n2 - The IOR of the medium being entered.
//		cosI - The precaculated dot between the ray direction and normal.
// Returns:
//		The normalized refraction vector.
glm::vec3 refract(const Ray &ray, const glm::vec3 &normal, float n1, float n2, float cosI) {
	float n = n1 / n2;
	float cosT2 = 1.0f - n * n * (1.0f - cosI * cosI);
	return glm::normalize(n * ray.direction + normal * (n * cosI - (float)sqrt(cosT2)));
}

// Calculates Fresnel using Schlick's approximation.
// Params:
//		incident - The incoming ray direction.
//		normal - The surface normal.
//		n1 - The refraction index of the exiting material.
//		n2 - The refraction index of the entering material.
//		cosI - The dot product between the incident and normal vectors.
float getReflectance(const glm::vec3 &incident, const glm::vec3 &normal, float n1, float n2, float cosI) {
	float n = n1 / n2;
	float cosT2 = 1.0f - n * n * (1.0f - cosI * cosI);
	// Total internal reflection.
	if (cosT2 < 0.0f) {
		return 1.0f;
	}
	float R0 = pow((n2 - 1)/(n2+1), 2);
	float schlick = R0 + (1-R0) * pow(1 - cosI, 5);
	return schlick;
}

// Calculates attentuation using Beer-Lambert.
// Params:
//		ray - The incident ray.
//		t - The intersection parameter.
//		material - The material of the object hit.
// Returns:
//		The absorption color.
glm::vec4 calculateBeerLambert(const Ray &ray, float t, const Material &material) {
	glm::vec4 absorbance = std::abs(t - ray.tmin) * -material.absorbance * material.refraction;
	absorbance.r = std::exp(absorbance.r);
	absorbance.g = std::exp(absorbance.g);
	absorbance.b = std::exp(absorbance.b);
	if (material.absorbance > 0.0f) {
		int i = 3 + 5;
	}
	return absorbance;
}

// Calculates the amount to attenuate shading due to shadows.  Shadow rays are intersected with every object and the light is attenuated through any 
// transparent objects.
// Params: 
//		scene - The scene to render.
//		ray - The incident ray of the original intersection.
//		position - The position of intersection.
//		light - The light to render shadows against.
//		prevRefractIndex - The refraction index of the previous medium.
glm::vec4 calculateShadowAttenuation(const Scene &scene, const Ray &ray, const glm::vec3 &position, Light *light, float prevRefractIndex) {
	glm::vec4 totalIntensity;
	int numSamples = (int)light->geometricObj->sampler->numSamples;
	for (int i = 0; i < numSamples; i++) {
		glm::vec3 lightPos = light->geometricObj->sample(i);
		glm::vec3 lightDir = glm::normalize(lightPos - position);
		Ray shadowRay(position + lightDir * EPSILON, lightDir, 0.0f, ray.tmax);
		std::vector<Intersection> intersections;
		intersectAll(scene, shadowRay, intersections);
		float distToLight = glm::length(lightPos - position);
		glm::vec4 sampleIntensity = glm::vec4(1.0f);
		for (int hit = 0; hit < intersections.size(); hit++) {
			float tHit = intersections[hit].tHit;
			Instance *objectHit = intersections[hit].objectHit;
			glm::vec3 positionHit = position + lightDir * tHit;
			float distToObject = glm::length(positionHit - position);
			if (distToObject < distToLight) {
				if (objectHit->material.isDielectric && objectHit->material.isRefractive()) {
					glm::vec3 normalHit = objectHit->getNormal(positionHit);
					float cosI = glm::dot(lightDir, normalHit);
					float refl;
					glm::vec4 absorbance = calculateBeerLambert(ray, tHit, objectHit->material) * objectHit->material.refraction;
					if (cosI < 0.0f) {
						refl = getReflectance(lightDir, normalHit, prevRefractIndex, objectHit->material.ior, -cosI);
					}
					else {
						refl = getReflectance(lightDir, -normalHit, objectHit->material.ior, prevRefractIndex, cosI);
					}
					sampleIntensity = (1 - refl) * absorbance * sampleIntensity;
				}
				else {
					sampleIntensity = glm::vec4(0.0f);
					break;
				}
			}
			else {
				break;
			}
		}
		totalIntensity += sampleIntensity;
	}

	return totalIntensity / (float) numSamples;
}

// Tests a ray for intersection with objects in a given scene.
// Params:
//		scene - The scene being rendered.
//		ray - The ray to test.
//		objectHit - Will return an object that was hit by the ray, or NULL.
//		tClosestHit - Returns the t value of the closest hit object.
void intersectClosest(const Scene &scene, const Ray &ray, Instance *&objectHit, float &tClosestHit) {
	std::vector<Instance*> instances = scene.objects;
	for (int i = 0; i < instances.size(); i++) {
		Instance *object = instances[i];
		float tHit;
        if (object->intersect(ray, tHit) == true) {
            if (tHit < tClosestHit) {
                tClosestHit = tHit;
                objectHit = instances[i];
            }
        }
    }
}

// Intersects a ray with every object in the scene, returning references to every object it hits.
// Params:
//		scene - The scene to trace.
//		ray - The ray to test for intersection with.
//		intersections - Returns the list of sorted intersections.
void intersectAll(const Scene &scene, const Ray &ray, std::vector<Intersection> &intersections) {
	std::vector<Instance*> instances = scene.objects;
	for (int i = 0; i < instances.size(); i++) {
		Instance *object = instances[i];
		float tHit;
		if (object->intersect(ray, tHit) == true) {
			Intersection intersection(object, tHit);
			intersections.push_back(intersection);
		}
	}
	std::sort(intersections.begin(), intersections.end());
}

// Sets a pixel in the final image.
// Params:
//		image - An array of 32-bit pixels represented as unsigned chars.  
//		x - The x coordinate of the pixel.
//		y - The y coordinate of the pixel.
//		color - The final color of the pixel.
void setPixel(std::vector<unsigned char> &image, int x, int y, glm::vec4 color) {
	color = glm::clamp(color, 0.0f, 1.0f);
	image[4 * IMAGE_WIDTH * y + 4 * x + 0] = (unsigned char)(color.x * 255);
	image[4 * IMAGE_WIDTH * y + 4 * x + 1] = (unsigned char)(color.y * 255);
	image[4 * IMAGE_WIDTH * y + 4 * x + 2] = (unsigned char)(color.z * 255);
	image[4 * IMAGE_WIDTH * y + 4 * x + 3] = 255;
}

// Write the final image to disk.
// Params:
//		filename - The filename of the image.
//		image - An array of 32-bit pixels represented as unsigned chars.  
void writeImageToDisk(const char* filename, std::vector<unsigned char>& image) {
  //Encode the image
  unsigned error = lodepng::encode(filename, image, IMAGE_WIDTH, IMAGE_HEIGHT);

  //if there's an error, display it
  if(error) std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
}
