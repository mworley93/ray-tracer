#include "Ray.h"

Ray::Ray(float x, float y, int width, int height, const Camera &camera) {
	float ndcX = 2 * (x + 0.5f) / (float)width - 1;
	float ndcY = 1 - 2 * (y + 0.5f) / (float)height;
	float aspect = (float)width / (float)height;
	float fovAngle = glm::tan(camera.getFOV()*0.5f);
	float camX = ndcX * aspect * fovAngle;
	float camY = ndcY * fovAngle;
	glm::mat4 camToWorld;
	camera.getCameraToWorldMat(camToWorld);
	glm::vec4 rayPoint = camToWorld * glm::vec4(camX, camY, -1.0f, 1.0f);
	this->origin = glm::vec3(camToWorld * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	this->direction = glm::normalize(glm::vec3(rayPoint) - origin);
	this->tmin = camera.getNearPlane();
	this->tmax = camera.getFarPlane();
}

Ray::Ray(glm::vec3 origin, glm::vec3 direction, float tmin, float tmax) {
	this->origin = origin;
	this->direction = direction;
	this->tmin = tmin;
	this->tmax = tmax;
}

Ray::~Ray(void)
{
}
