#pragma once
#include "sampler.h"

// This subclass implements jittered sampling.  In jittered sampling, a grid based on the number of samples is created and, like regular sampling, 
// a sample for each grid square is returned.  However, the point in each grid square is "jittered", or randomly offset.
class JitteredSampler :
	public Sampler
{
public:
	JitteredSampler(void);
	JitteredSampler(int samplesX, int samplesY);

	~JitteredSampler(void);

	glm::vec3 sampleUnitSquare(int sampleNum);

	glm::vec3 sampleUnitSphere(int sampleNum);
};

