#pragma once
#include "GeometricObject.h"

class Triangle :
	public GeometricObject
{
public:
	Triangle(void);
	~Triangle(void);
	bool intersect(const Ray &ray, float &tHit);
};

