#include "MatrixStack.h"


MatrixStack::MatrixStack(void)
	: mCurrentMatrix(1.0f) {
}

MatrixStack::MatrixStack(const glm::mat4 &currentMatrix) 
	: mCurrentMatrix(currentMatrix) {
}

MatrixStack::~MatrixStack(void)
{
}

// Returns the current matrix.
const glm::mat4 &MatrixStack::top() const {
	return mCurrentMatrix;
}

void MatrixStack::loadIdentity() {
	mCurrentMatrix = glm::mat4(1.0f);
}


void MatrixStack::translate(float x, float y, float z) {
	mCurrentMatrix = glm::translate(mCurrentMatrix, glm::vec3(x, y, z));
}


void MatrixStack::scale(float x, float y, float z) {
	mCurrentMatrix = glm::scale(mCurrentMatrix, glm::vec3(x, y, z));
}

void MatrixStack::rotate(float angle, float x, float y, float z) {
	mCurrentMatrix = glm::rotate(mCurrentMatrix, angle, glm::vec3(x, y, z));
}

void MatrixStack::pushStack() {
	mMatrixStack.push(mCurrentMatrix);
}

void MatrixStack::popStack() {
	mCurrentMatrix = mMatrixStack.top();
	mMatrixStack.pop();
}