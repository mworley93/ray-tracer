#include "Sphere.h"
#include <algorithm>

Sphere::Sphere(void) {
	mCenter = glm::vec3(0, 0, 0);
	mRadius = 1;
	mRadiusSquared = 1;
}

Sphere::Sphere(glm::vec3 center, float radius) {
	mCenter = center;
	mRadius = radius;
	mRadiusSquared = pow(mRadius, 2);
}

Sphere::~Sphere(void)
{
}

bool solveQuadratic(const float &a, const float &b, const float &c, float &x0, float &x1)
{
    float discr = b * b - 4 * a * c;
    if (discr < 0) return false;
    else if (discr == 0) x0 = x1 = - 0.5f * b / a;
    else {
        float q = (b > 0) ?
            -0.5f * (b + sqrt(discr)) :
            -0.5f * (b - sqrt(discr));
        x0 = q / a;
        x1 = c / q;
    }
    if (x0 > x1) std::swap(x0, x1);
    return true;
}

bool Sphere::intersect(const Ray &ray, float &tHit) {
	glm::vec3 L = ray.origin - mCenter;
	float a = glm::dot(ray.direction, ray.direction);
	float b = 2 * glm::dot(ray.direction, L);
	float c = glm::dot(L, L) - mRadiusSquared;

	float t0, t1;
    if (!solveQuadratic(a, b, c, t0, t1)) return false;

	// If ray origin is in the sphere, there is one intersection point. TODO: Check this...needed for refraction..
	if (t0 < 0) {
		if (t1 < ray.tmax && t1 > ray.tmin) {
				tHit = t1;
				return true;
			}
			else {
				return false;
		}
	}
	if (t0 > ray.tmax || t0 < ray.tmin) {
		return false;
	}
	else {
		tHit = t0;
	}
	return true;
}

glm::vec3 Sphere::getNormal(glm::vec3 position) {
	return glm::normalize(position - mCenter);
}

glm::vec3 Sphere::sample(int sampleNum, Sampler *sampler) {
	// TODO: Replace this with a different sampler mapping.
	return sampler->sampleUnitSphere(sampleNum);
}